#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct table
{
	char *cName;	//Название маршрута
	float fLine;	//Протяженность
	int iStan;		//Количество остановок
	float fPric;	//Стоимость путевки
};

int main()		//Вариант 8
{
	int i = 0, j = 0;
	int iKM;	//Количество маршрутов
	char cBuf[50];	//Буфер
	struct table *tVoyager;
	struct table tB;

	printf("Введит количество маршрутов: ");
	scanf("%d", &iKM);
	tVoyager = (struct table *)malloc(sizeof(struct table)*iKM);
	printf("Введите данные:\n");

	for (i = 0; i < iKM; i++)
	{
		printf ("Название маршрута №%d: ", i+1);
		scanf("%s", cBuf);
		tVoyager[i].cName = (char *)malloc(sizeof(char)*strlen(cBuf));
       	strcpy(tVoyager[i].cName, cBuf);
       	printf ("Протяжённость маршрута №%d: ", i+1);
		scanf("%f", &tVoyager[i].fLine);
		printf ("Количество остановок на маршруте №%d: ", i+1);
		scanf("%d", &tVoyager[i].iStan);
		printf ("КСтоимость маршрута №%d: ", i+1);
		scanf("%f", &tVoyager[i].fPric);
	}
	for ( i = 0; i < iKM - 1; i++)
	{
		for ( j = i; j < iKM; j++)
		{
			if(tVoyager[i].fPric <= tVoyager[j].fPric)
			{
				tB = tVoyager[i];
				tVoyager[i] = tVoyager[j];
				tVoyager[j] = tB;
			}
		}
	}
    system("clear");
    for(i = 0; i < iKM; i++)
    {
        printf("\n");
        printf("Маршрут: %s\n", tVoyager[i].cName);
        printf("Длина маршрута (в км): %.2f\n", tVoyager[i].fLine);
        printf("Количество остановок: %d\n", tVoyager[i].iStan);
        printf("Стоимость маршрута: %.2f\n", tVoyager[i].fPric);
        printf("\n");
    }
	free(tVoyager);
	return 0;
}
