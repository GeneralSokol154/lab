#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct table
{
	char *cName;	//Название маршрута
	float fLine;	//Протяженность
	int iStan;		//Количество остановок
	float fPric;	//Стоимость путевки
};

int input_begin(int iKM)
{
    printf("Введит количество маршрутов: ");
	scanf("%d", &iKM);
	return iKM;
}

struct table* input(struct table* tVoyager, int iKM)
{
    int i = 0;
    char cBuf[50];
    printf("Введите данные:\n");
	for (i = 0; i < iKM; i++)
	{
		printf ("Название маршрута №%d: ", i+1);
		scanf("%s", cBuf);
		tVoyager[i].cName = (char *)malloc(sizeof(char)*strlen(cBuf));
       	strcpy(tVoyager[i].cName, cBuf);
       	printf ("Протяжённость маршрута №%d: ", i+1);
		scanf("%f", &tVoyager[i].fLine);
		printf ("Количество остановок на маршруте №%d: ", i+1);
		scanf("%d", &tVoyager[i].iStan);
		printf ("КСтоимость маршрута №%d: ", i+1);
		scanf("%f", &tVoyager[i].fPric);
	}
	return tVoyager;
}

static int sort(const void *i, const void *j)
{
	struct table *pa = i;
    struct table *pb = j;
    return pb->fPric - pa->fPric;
}

void output(struct table* tVoyager, int iKM)
{
    int i = 0;
    for(i = 0; i < iKM; i++)
    {
        printf("\n");
        printf("Маршрут: %s\n", tVoyager[i].cName);
        printf("Длина маршрута (в км): %.2f\n", tVoyager[i].fLine);
        printf("Количество остановок: %d\n", tVoyager[i].iStan);
        printf("Стоимость маршрута: %.2f\n", tVoyager[i].fPric);
        printf("\n");
    }
}

int main()		//Вариант 8
{
	int i = 0;
	int iKM = 0;	//Количество маршрутов
	struct table *tVoyager;

    iKM = input_begin(iKM);
	tVoyager = (struct table *)malloc(sizeof(struct table)*iKM);
	input(tVoyager, iKM);
	qsort(tVoyager, iKM, sizeof(struct table), sort);
    system("clear");
    output(tVoyager, iKM);
    for(i = 0; i < iKM; i++)
    {
        free(tVoyager[i].cName);
    }
	free(tVoyager);
	return 0;
}
