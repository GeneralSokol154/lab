#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int enter()
{	
	int iKS = 0;
	printf("Введите количество строк: ");
	//char cBuf[255]={0};
	scanf("%d", &iKS);
	//gets(cBuf);			//Считывание строки
	//iKS = atoi(cBuf);	//Преобразование из char в int
	return iKS;
}

char** Str(int iKS, char** cStr)
{
	int i = 0;
	char cBuf[255]={0};
	for (i = 0; i < iKS; i++)
	{
		printf("Строка номер %d: ", i+1);
		scanf("%s", cBuf);
		//gets(cBuf);
		cStr[i] = (char *)malloc(sizeof(char)*strlen(cBuf));
       	strcpy(cStr[i], cBuf);
	}
	return cStr;
}

char** sort (char** cStr, int i, int iKS)
{
	int j = 0;
	char *cB;
	for ( i = 0; i < iKS - 1; i++)
	{
		for ( j = i; j < iKS; j++)
		{
			if(strlen(cStr[i]) <= strlen(cStr[j]))
			{
				cB = cStr[i];
				cStr[i] = cStr[j];
				cStr[j] = cB;
			}
		}
	}
	return(cStr);
}

void output(char** cStr, int iKS)
{
	int i = 0;
	for (i = 0; i < iKS; i++)
	{
		printf("Строка номер %d: %s\n", i, cStr[i]);
	}
}

int main()
{
	int i = 0;
	int iKS = 0;
	char **cStr;
	
	iKS = enter();
	cStr = (char **)malloc(sizeof(char *)*iKS);
	cStr = Str(iKS, cStr);
	sort(cStr, i, iKS);
	output(cStr, iKS);

	for ( i = 0; i < iKS; i++)
	{
		free(cStr[i]);
	}
	free(cStr);
	
	return(0);
}
